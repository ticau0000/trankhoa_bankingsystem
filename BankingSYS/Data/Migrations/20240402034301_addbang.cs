﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BankingSYS.Data.Migrations
{
    /// <inheritdoc />
    public partial class addbang : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Logs_Transactions_TransactionID",
                table: "Logs");

            migrationBuilder.DropForeignKey(
                name: "FK_Reports_Accounts_AccountID",
                table: "Reports");

            migrationBuilder.DropForeignKey(
                name: "FK_Reports_Logs_LogsID",
                table: "Reports");

            migrationBuilder.DropForeignKey(
                name: "FK_Reports_Transactions_TransactionID",
                table: "Reports");

            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_Customers_CustomerID",
                table: "Transactions");

            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_Employees_EmployeeID",
                table: "Transactions");

            migrationBuilder.DropIndex(
                name: "IX_Transactions_EmployeeID",
                table: "Transactions");

            migrationBuilder.DropIndex(
                name: "IX_Reports_AccountID",
                table: "Reports");

            migrationBuilder.DropIndex(
                name: "IX_Reports_TransactionID",
                table: "Reports");

            migrationBuilder.DropIndex(
                name: "IX_Logs_TransactionID",
                table: "Logs");

            migrationBuilder.DropIndex(
                name: "IX_Accounts_CustomerId",
                table: "Accounts");

            migrationBuilder.DropColumn(
                name: "EmployeeID",
                table: "Transactions");

            migrationBuilder.DropColumn(
                name: "AccountID",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "TransactionID",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "TransactionID",
                table: "Logs");

            migrationBuilder.RenameColumn(
                name: "CustomerID",
                table: "Transactions",
                newName: "CustomerId");

            migrationBuilder.RenameIndex(
                name: "IX_Transactions_CustomerID",
                table: "Transactions",
                newName: "IX_Transactions_CustomerId");

            migrationBuilder.RenameColumn(
                name: "LogsID",
                table: "Reports",
                newName: "LogsId");

            migrationBuilder.RenameIndex(
                name: "IX_Reports_LogsID",
                table: "Reports",
                newName: "IX_Reports_LogsId");

            migrationBuilder.AlterColumn<int>(
                name: "CustomerId",
                table: "Transactions",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "EmployeesId",
                table: "Transactions",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "LogsId",
                table: "Reports",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "AccountsId",
                table: "Reports",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TransactionsId",
                table: "Reports",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TransactionsId",
                table: "Logs",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_EmployeesId",
                table: "Transactions",
                column: "EmployeesId");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_AccountsId",
                table: "Reports",
                column: "AccountsId");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_TransactionsId",
                table: "Reports",
                column: "TransactionsId");

            migrationBuilder.CreateIndex(
                name: "IX_Logs_TransactionsId",
                table: "Logs",
                column: "TransactionsId");

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_CustomerId",
                table: "Accounts",
                column: "CustomerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Logs_Transactions_TransactionsId",
                table: "Logs",
                column: "TransactionsId",
                principalTable: "Transactions",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Reports_Accounts_AccountsId",
                table: "Reports",
                column: "AccountsId",
                principalTable: "Accounts",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Reports_Logs_LogsId",
                table: "Reports",
                column: "LogsId",
                principalTable: "Logs",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Reports_Transactions_TransactionsId",
                table: "Reports",
                column: "TransactionsId",
                principalTable: "Transactions",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_Customers_CustomerId",
                table: "Transactions",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_Employees_EmployeesId",
                table: "Transactions",
                column: "EmployeesId",
                principalTable: "Employees",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Logs_Transactions_TransactionsId",
                table: "Logs");

            migrationBuilder.DropForeignKey(
                name: "FK_Reports_Accounts_AccountsId",
                table: "Reports");

            migrationBuilder.DropForeignKey(
                name: "FK_Reports_Logs_LogsId",
                table: "Reports");

            migrationBuilder.DropForeignKey(
                name: "FK_Reports_Transactions_TransactionsId",
                table: "Reports");

            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_Customers_CustomerId",
                table: "Transactions");

            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_Employees_EmployeesId",
                table: "Transactions");

            migrationBuilder.DropIndex(
                name: "IX_Transactions_EmployeesId",
                table: "Transactions");

            migrationBuilder.DropIndex(
                name: "IX_Reports_AccountsId",
                table: "Reports");

            migrationBuilder.DropIndex(
                name: "IX_Reports_TransactionsId",
                table: "Reports");

            migrationBuilder.DropIndex(
                name: "IX_Logs_TransactionsId",
                table: "Logs");

            migrationBuilder.DropIndex(
                name: "IX_Accounts_CustomerId",
                table: "Accounts");

            migrationBuilder.DropColumn(
                name: "EmployeesId",
                table: "Transactions");

            migrationBuilder.DropColumn(
                name: "AccountsId",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "TransactionsId",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "TransactionsId",
                table: "Logs");

            migrationBuilder.RenameColumn(
                name: "CustomerId",
                table: "Transactions",
                newName: "CustomerID");

            migrationBuilder.RenameIndex(
                name: "IX_Transactions_CustomerId",
                table: "Transactions",
                newName: "IX_Transactions_CustomerID");

            migrationBuilder.RenameColumn(
                name: "LogsId",
                table: "Reports",
                newName: "LogsID");

            migrationBuilder.RenameIndex(
                name: "IX_Reports_LogsId",
                table: "Reports",
                newName: "IX_Reports_LogsID");

            migrationBuilder.AlterColumn<int>(
                name: "CustomerID",
                table: "Transactions",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EmployeeID",
                table: "Transactions",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "LogsID",
                table: "Reports",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AccountID",
                table: "Reports",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TransactionID",
                table: "Reports",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TransactionID",
                table: "Logs",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_EmployeeID",
                table: "Transactions",
                column: "EmployeeID");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_AccountID",
                table: "Reports",
                column: "AccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_TransactionID",
                table: "Reports",
                column: "TransactionID");

            migrationBuilder.CreateIndex(
                name: "IX_Logs_TransactionID",
                table: "Logs",
                column: "TransactionID");

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_CustomerId",
                table: "Accounts",
                column: "CustomerId",
                unique: true,
                filter: "[CustomerId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Logs_Transactions_TransactionID",
                table: "Logs",
                column: "TransactionID",
                principalTable: "Transactions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Reports_Accounts_AccountID",
                table: "Reports",
                column: "AccountID",
                principalTable: "Accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Reports_Logs_LogsID",
                table: "Reports",
                column: "LogsID",
                principalTable: "Logs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Reports_Transactions_TransactionID",
                table: "Reports",
                column: "TransactionID",
                principalTable: "Transactions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_Customers_CustomerID",
                table: "Transactions",
                column: "CustomerID",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_Employees_EmployeeID",
                table: "Transactions",
                column: "EmployeeID",
                principalTable: "Employees",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
