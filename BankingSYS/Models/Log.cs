﻿using BankingSYS.Models;

namespace KTTuan8LTWeb.Models
{
    public class Log
    {
        public int Id { get; set; }
        public DateTime LoginDate { get; set; }
        public DateTime LoginTime { get; set; }
        //public int TransactionID { get; set; }
        public Transaction? Transactions { get; set; }
        //public ICollection<Report>? Reports { get; set; }
    }
}
