﻿using KTTuan8LTWeb.Models;

namespace BankingSYS.Models
{
    public class Report
    {
        public int Id { get; set; }
        public string ReportName { get; set; }
        public DateTime ReportDate { get; set; }
        //public int AccountID { get; set; }
        public Account? Accounts { get; set; }
        //public int LogsID { get; set; }
        public Log? Logs { get; set; }
        //public int TransactionID { get; set; }
        public Transaction? Transactions { get; set; }
    }
}
