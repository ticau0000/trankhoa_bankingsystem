﻿using KTTuan8LTWeb.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace BankingSYS.Models
{
    public class Account
    {
        public int Id { get; set; }
        public string Name { get; set; }

        //public List<Report> Reports { get; set; }
        //public int? CustomerId { get; set; }
        
        public Customer? Customer { get; set; }
    }
}
