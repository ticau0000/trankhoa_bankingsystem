﻿using BankingSYS.Models;

namespace KTTuan8LTWeb.Models
{
    public class Transaction
    {
        public int Id { get; set; }
        public string Name { get; set; }
        //public int EmployeeID { get; set; }
        public Employee? Employees { get; set; }
        //public int CustomerID { get; set; }
        public Customer? Customer { get; set; }
        //public ICollection<Log>? Logs { get; set; }
        //public ICollection<Report>? Reports { get; set; }

    }
}
